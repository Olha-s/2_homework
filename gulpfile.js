const gulp = require('gulp');
const clean = require('gulp-clean');
const concat = require('gulp-concat');
const minifyjs = require('gulp-js-minify');
const uglify = require('gulp-uglify');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();

const paths= {
    src:{
        styles: 'src/scss/**/*.scss',
        script: 'src/js/**/*.js',
        img:'src/img/**/*',
        html: 'src/index.html',
    },
    dist:{
        root: 'dist',
        styles: 'dist/css',
        script: 'dist/js',
        img:'dist/img',
        html: 'dist/',
    }
};
const cleanDist = () => (
    gulp.src('dist', {allowEmpty: true})
        .pipe(clean())
);

const buildJS = () => (
    gulp.src(paths.src.script)
        .pipe(concat('script.min.js'))
        .pipe(minifyjs())
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist.script))
);


const buildSCSS = () =>(
    gulp.src(paths.src.styles)
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('style.min.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(paths.dist.styles))
);
const imageMIN = () =>(
    gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest(paths.dist.img))
);

const watcher = () => {
    browserSync.init({
        server: {
             baseDir: "./"
        }
    });
    gulp.watch(paths.src.styles, buildSCSS).on('change', browserSync.reload);
    gulp.watch(paths.src.script, buildJS).on('change', browserSync.reload);
};
gulp.task('build', gulp.series(
    cleanDist,
    buildSCSS,
    buildJS,
    imageMIN
));
gulp.task('dev', gulp.series(
    buildSCSS,
    buildJS,
    watcher
));